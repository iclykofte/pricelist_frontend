# Price List Frontend

Simple and yet powerful application to manage your price lists offered by suppliers, wholesalers, and dealers. 
It merges all prices, and provides a easy to use gui for user. If you have frequently changing price lists this app is for you.

## Technology:
python and pyqt4 for GUIs, and RESTful calls for communication. HTTP auth for authentication.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Although you can develop on your physical environment, I highly recommend using virtual environment for development.

Requirements are listed in requirements.txt and can be installed with pip command.

As it uses pyqt4, I recommend installing pyqt4 system wide (not using pip). For instance ubuntu users might install it
with the command:

sudo apt-get install python-qt4

if you're installing software on system (not virtual environment) you might want to install pip as well:

sudo apt-get install python-pip

I also recommend upgrading some critical python packages:

sudo pip install --upgrade setuptools<br>
sudo pip install --upgrade pip

If you have fruther difficulties with some of requirements try globally installing them (not on virtualenv), and enable global site packages on virtual environment.

### Installing

pyinstaller can be used to package resulting application to an executable file with related dependencies.

Another way that I highly recommend is to use setuptools to install the software. Following command will install the software to system (if you're using virtual environment it will be installed on virtual environment).

sudo python setup.py install

executable will be installed on the path (for ubuntu users, other distros may be different): /usr/local/bin/pricelist_gui

## Running the tests

Unfortunately there is no tests yet, this was a very quick application to write (less than a week). Yes it is not a good programming practice, but I'll add them as I find a break.

## Deployment

tested with python 2.7.

Deploy with pyinstaller outputs, and run the executable.

## Built With

python 2.7+

* [pyQt4](https://wiki.python.org/moin/PyQt4) - Multiplatform GUI Development Toolkit with python
* [xlrd](http://xlrd.readthedocs.io/en/latest/api.html) - Excel files reading library
* [xlwt](https://pypi.python.org/pypi/xlwt) - Excel files writing library

## Contributing

Please feel free to contact me.

## Versioning

I use [git](https://git-scm.com/) for versioning.  

## Authors

* **Hayati Gonultas** - *Initial work* - [iclykofte](https://bitbucket.org/iclykofte/)

## License

This project is licensed under the MIT License.