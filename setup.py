from setuptools import setup, find_packages

setup(
    name='PriceList Fontend',
    version='0.1',
    url='https://bitbucket.org/iclykofte/pricelist_frontend',
    license='MIT',
    author='Hayati Gonultas',
    author_email='hayati.gonultas@gmail.com',
    description='Simple price list management application (GUI module) written in pyqt4',
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    install_requires=[
        'xlrd',
        'xlwt',
        'requests'
    ],
    entry_points={'gui_scripts':['pricelist_gui=pricelist_frontend.__main__:main']},
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers, Users',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2'
    ]
)