from PyQt4 import QtGui
from PyQt4 import QtCore
from ui_progresswindow import Ui_ProgressWindow

class ProgressWindow(QtGui.QWidget, Ui_ProgressWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setupUi(self)
        self.setWindowModality(QtCore.Qt.WindowModal)

    def setHeader(self, header):
        self.lblHeader.setText(header)

    def setDesc(self, desc):
        self.lblDesc.setText(desc)

    def setProgressMax(self, max):
        self.pbWork.setMaximum(max)

    def setProgressCurrent(self, val):
        self.pbWork.setValue(val)