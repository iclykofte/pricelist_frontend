# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/progresswindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ProgressWindow(object):
    def setupUi(self, ProgressWindow):
        ProgressWindow.setObjectName(_fromUtf8("ProgressWindow"))
        ProgressWindow.resize(608, 96)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/avatar.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        ProgressWindow.setWindowIcon(icon)
        self.verticalLayout = QtGui.QVBoxLayout(ProgressWindow)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.lblHeader = QtGui.QLabel(ProgressWindow)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.lblHeader.setFont(font)
        self.lblHeader.setObjectName(_fromUtf8("lblHeader"))
        self.verticalLayout.addWidget(self.lblHeader)
        self.lblDesc = QtGui.QLabel(ProgressWindow)
        self.lblDesc.setObjectName(_fromUtf8("lblDesc"))
        self.verticalLayout.addWidget(self.lblDesc)
        self.pbWork = QtGui.QProgressBar(ProgressWindow)
        self.pbWork.setProperty("value", 0)
        self.pbWork.setObjectName(_fromUtf8("pbWork"))
        self.verticalLayout.addWidget(self.pbWork)

        self.retranslateUi(ProgressWindow)
        QtCore.QMetaObject.connectSlotsByName(ProgressWindow)

    def retranslateUi(self, ProgressWindow):
        ProgressWindow.setWindowTitle(_translate("ProgressWindow", "Please Wait...", None))
        self.lblHeader.setText(_translate("ProgressWindow", "Please wait...", None))
        self.lblDesc.setText(_translate("ProgressWindow", "Processing file(s).", None))

import pricelist_rc
