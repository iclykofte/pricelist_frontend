import requests

from pricelist_frontend.model.user import user
from connection_info import connection


def get_auth_token(username, password):
    url = "%s/api/token" % connection.endpoint

    myResponse = requests.get(url, timeout=5, auth=(username, password))

    if (myResponse.ok):
        jData = myResponse.json()
        return jData
    else:
        myResponse.raise_for_status()


def change_password(username, password):
    url = "%s/user/update/%s" % (connection.endpoint, username)

    myResponse = requests.post(url,
                               '{"username":"%s", "password":"%s"}' % (username, password),
                               timeout=5,
                               auth=(user.auth_token, 'none'))

    if (myResponse.ok):
        return True
    else:
        myResponse.raise_for_status()
