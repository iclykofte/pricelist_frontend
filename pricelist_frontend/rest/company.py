import json
import requests

from pricelist_frontend.model.user import user
from pricelist_frontend.util.json_utils import CompanyEncoder
from connection_info import connection


def get_companies():
    url = "%s/companies" % connection.endpoint

    myResponse = requests.get(url, timeout=5, auth=(user.auth_token, 'none'))

    if (myResponse.ok):
        jData = myResponse.json()
        return jData
    else:
        myResponse.raise_for_status()


def add_company(company):
    url = "%s/company/add" % connection.endpoint

    myResponse = requests.post(url, json.dumps(company, cls=CompanyEncoder), timeout=5, auth=(user.auth_token, 'none'))

    if (myResponse.ok):
        return True
    else:
        myResponse.raise_for_status()


def delete_company(company):
    url = "%s/company/delete/%d" % (connection.endpoint, company.id)

    myResponse = requests.delete(url, timeout=5, auth=(user.auth_token, 'none'))

    if (myResponse.ok):
        return True
    else:
        myResponse.raise_for_status()


def update_company(company):
    url = "%s/company/update/%d" % (connection.endpoint, company.id)

    myResponse = requests.put(url, json.dumps(company, cls=CompanyEncoder), timeout=5, auth=(user.auth_token, 'none'))

    if (myResponse.ok):
        return True
    else:
        myResponse.raise_for_status()
