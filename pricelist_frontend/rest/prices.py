import json
import requests

from pricelist_frontend.model.user import user
from pricelist_frontend.util.json_utils import PriceListEncoder
from connection_info import connection


def update_pricelist(pricelist):
    url = "%s/pricelist/update/%d" % (connection.endpoint, pricelist.id)

    myResponse = requests.post(url, json.dumps(pricelist, cls=PriceListEncoder), auth=(user.auth_token, 'none'))

    if (myResponse.ok):
        return True
    else:
        myResponse.raise_for_status()


def get_matched_products(str):
    url = "%s/products/%s" % (connection.endpoint, str)

    myResponse = requests.get(url, auth=(user.auth_token, 'none'))

    if (myResponse.ok):
        jData = myResponse.json()
        return jData
    else:
        myResponse.raise_for_status()
