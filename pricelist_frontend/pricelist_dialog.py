from PyQt4 import QtCore
from PyQt4 import QtGui

from model.price_list import PriceList
from ui_pricelist_dialog import Ui_PriceListlDialog


class PriceListDialog(QtGui.QDialog, Ui_PriceListlDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

        self.company = None

        self.pricelist = None

        self.connect(self.pbOk, QtCore.SIGNAL('clicked()'), self.on_ok_clicked)
        self.connect(self.pbCancel, QtCore.SIGNAL('clicked()'), self.on_cancel_clicked)
        self.connect(self.pbChooseExcelFile, QtCore.SIGNAL('clicked()'), self.on_choose_excel_clicked)


    def set_company(self, company):
        self.company = company


    def set_pricelist(self, pricelist):
        self.pricelist = pricelist
        self.update_pricelist_gui()


    def update_pricelist_gui(self):
        if self.pricelist != None:
            self.lblExcelFile.setText(self.pricelist.filename)
            self.sbProductsRow.setValue(self.pricelist.products_row + 1)
            self.leNameColumn.setText(self._to_excel_char(self.pricelist.product_name_col))
            self.lePriceColumn.setText(self._to_excel_char(self.pricelist.price_col))
            self.sbPageNo.setValue(self.pricelist.pageno+1)


    def check_pricelist(self):
        if self.pricelist.filename == None or self.pricelist.filename == "" or self.pricelist.product_name_col == self.pricelist.price_col:
            QtGui.QMessageBox.information(self, 'Please check your entry',
                                          'Excel file not specified or price and product columns are same!')
            return False

        return True


    def _to_excel_number(self, entry):
        return ord(entry.upper())-ord('A')


    def _to_excel_char(self, entry):
        return chr(ord('A')+entry)


    def on_ok_clicked(self):
        self.pricelist = PriceList()

        self.pricelist.filename = unicode(self.lblExcelFile.text())
        self.pricelist.pageno = self.sbPageNo.value() - 1 # starting from 0 fixation
        self.pricelist.products_row = self.sbProductsRow.value() - 1 # starting from 0 fixation
        self.pricelist.product_name_col = self._to_excel_number(unicode(self.leNameColumn.text()))
        self.pricelist.price_col = self._to_excel_number(unicode(self.lePriceColumn.text()))

        if self.check_pricelist():
            self.accept()


    def on_cancel_clicked(self):
        self.reject()


    def on_choose_excel_clicked(self):
        self.filename = QtGui.QFileDialog.getOpenFileName(self, 'Select price list',
                                                          QtCore.QDir.homePath(), 'Excel (*.xls *.xlsx)')
        self.lblExcelFile.setText(self.filename)
