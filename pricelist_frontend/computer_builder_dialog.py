from PyQt4 import QtCore
from PyQt4 import QtGui

from ui_computer_builder_dialog import Ui_ComputerBuilderDialog
from model.computer import computer
from util.xls_save import save_computer

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class ComputerBuilderDialog(QtGui.QDialog, Ui_ComputerBuilderDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

        self.twComponents.verticalHeader().setDefaultSectionSize(56);

        header = QtCore.QStringList()
        header.append('Component')
        header.append('Price')
        header.append('Delete')
        self.twComponents.setHorizontalHeaderLabels(header)
        h = self.twComponents.horizontalHeader()
        h.setResizeMode(0, QtGui.QHeaderView.Stretch)
        h.setResizeMode(1, QtGui.QHeaderView.Interactive)

        self.deDate.setDate(QtCore.QDate.currentDate())

        self.connect(self.pbSave, QtCore.SIGNAL('clicked()'), self.on_save_clicked)
        self.connect(self.pbClose, QtCore.SIGNAL('clicked()'), self.on_close_clicked)
        self.connect(self.pbClearAll, QtCore.SIGNAL('clicked()'), self.on_clearall_clicked)

        self.deleteBtnList = []

        self.update_components_gui()

    def get_centered_cellbutton_widget(self, pb_icon):
        self.wedit = QtGui.QWidget()
        self.buttonlayout = QtGui.QHBoxLayout()
        self.buttonlayout.setAlignment(QtCore.Qt.AlignHCenter)
        self.pb = QtGui.QPushButton()
        self.pb.setFixedSize(QtCore.QSize(40, 40))
        self.pb.setIcon(QtGui.QIcon(_fromUtf8(pb_icon)))
        self.pb.setIconSize(QtCore.QSize(32, 32))
        self.buttonlayout.addWidget(self.pb)
        self.wedit.setLayout(self.buttonlayout)

        return self.wedit, self.pb

    def update_components_gui(self):
        self.twComponents.clearContents()
        self.deleteBtnList = []
        self.twComponents.setRowCount(len(computer.components))
        for i, p in enumerate(computer.components):
            item = QtGui.QTableWidgetItem(p.product_name)
            self.twComponents.setItem(i, 0, item)

            item = QtGui.QTableWidgetItem(p.price)
            self.twComponents.setItem(i, 1, item)

            self.deleteWidget, self.pDelete = self.get_centered_cellbutton_widget(":/images/res/delete-icon.png")
            self.connect(self.pDelete, QtCore.SIGNAL('clicked()'), self.delete_component)
            self.twComponents.setCellWidget(i, 2, self.deleteWidget)

            self.deleteBtnList.append(self.pDelete)

    def delete_component(self):
        row = self.deleteBtnList.index(self.sender())
        computer.remove_component(row)
        self.update_components_gui()

    def on_save_clicked(self):
        xlfile = QtGui.QFileDialog.getSaveFileName(self, "Save File", "iclykofte.xlsx", filter="Excel (*.xlsx *.xls)")
        save_computer(computer, xlfile, self.leName.text(), self.deDate.text())
        self.accept()

    def on_close_clicked(self):
        self.reject()

    def on_clearall_clicked(self):
        computer.clear_components()
        self.update_components_gui()
