# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/sales_dialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_SalesSettingsDialog(object):
    def setupUi(self, SalesSettingsDialog):
        SalesSettingsDialog.setObjectName(_fromUtf8("SalesSettingsDialog"))
        SalesSettingsDialog.resize(410, 187)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/avatar.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        SalesSettingsDialog.setWindowIcon(icon)
        SalesSettingsDialog.setStyleSheet(_fromUtf8("QFrame#frame{\n"
"border: 2px solid;\n"
"    border-radius: 6px;\n"
"}"))
        self.verticalLayout = QtGui.QVBoxLayout(SalesSettingsDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.frame = QtGui.QFrame(SalesSettingsDialog)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.gridLayout = QtGui.QGridLayout(self.frame)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.sbSalesPercentage = QtGui.QSpinBox(self.frame)
        self.sbSalesPercentage.setObjectName(_fromUtf8("sbSalesPercentage"))
        self.gridLayout.addWidget(self.sbSalesPercentage, 0, 1, 1, 1)
        spacerItem = QtGui.QSpacerItem(137, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 2, 1, 1)
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.leUsd = QtGui.QLineEdit(self.frame)
        self.leUsd.setObjectName(_fromUtf8("leUsd"))
        self.gridLayout.addWidget(self.leUsd, 1, 1, 1, 2)
        self.verticalLayout.addWidget(self.frame)
        spacerItem1 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.pbCancel = QtGui.QPushButton(SalesSettingsDialog)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/cancel-icon-47512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbCancel.setIcon(icon1)
        self.pbCancel.setObjectName(_fromUtf8("pbCancel"))
        self.horizontalLayout.addWidget(self.pbCancel)
        self.pbOk = QtGui.QPushButton(SalesSettingsDialog)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/ok-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbOk.setIcon(icon2)
        self.pbOk.setObjectName(_fromUtf8("pbOk"))
        self.horizontalLayout.addWidget(self.pbOk)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(SalesSettingsDialog)
        QtCore.QMetaObject.connectSlotsByName(SalesSettingsDialog)

    def retranslateUi(self, SalesSettingsDialog):
        SalesSettingsDialog.setWindowTitle(_translate("SalesSettingsDialog", "Sales Settings Dialog", None))
        self.label.setText(_translate("SalesSettingsDialog", "Sales percentage:", None))
        self.label_2.setText(_translate("SalesSettingsDialog", "USD ($):", None))
        self.pbCancel.setText(_translate("SalesSettingsDialog", "Cancel", None))
        self.pbOk.setText(_translate("SalesSettingsDialog", "OK", None))

import pricelist_rc
