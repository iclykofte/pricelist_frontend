class Computer:
    def __init__(self, components=[]):
        self.components = components

    def clear_components(self):
        self.components = []

    def add_component(self, price):
        self.components.append(price)

    def remove_component(self, idx):
        del self.components[idx]


computer = Computer()
