from os.path import isfile

import xlrd
from xlrd.sheet import XL_CELL_EMPTY

from pricelist_frontend.model.price import Price

from errno import ENOENT

class PriceList:
    def __init__(self, id=None, filename=None, products_row=None, product_name_col=None, price_col=None, pageno=0):
        self.id = id
        self.filename = filename
        self.products_row = products_row
        self.product_name_col = product_name_col
        self.price_col = price_col
        self.pageno = pageno
        self.__pricelist__ = True

    def get_excel_sheet_object(self, fname, idx=0):
        if not isfile(fname):
            print ('File does not exist: ', fname)
            raise IOError(ENOENT, 'File not exist', fname)

        # Open the workbook and 1st sheet
        xl_workbook = xlrd.open_workbook(fname)
        xl_sheet = xl_workbook.sheet_by_index(idx)
        print (40 * '-' + 'nRetrieved worksheet: %s' % xl_sheet.name)

        return xl_sheet

    def get_prices(self):
        xl_sheet = self.get_excel_sheet_object(self.filename, self.pageno)
        self.prices = []
        # Iterate through rows, and print out the column values
        row_vals = []
        for row_idx in range(self.products_row, xl_sheet.nrows):
            cell_product_name = xl_sheet.cell(row_idx, self.product_name_col)
            cell_price = xl_sheet.cell(row_idx, self.price_col)
            if xl_sheet.cell_type(row_idx, self.price_col) == XL_CELL_EMPTY:
                continue

            p = Price(product_name=cell_product_name.value,
                      price=cell_price.value)
            self.prices.append(p)

        return self.prices

