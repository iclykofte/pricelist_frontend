class Price:
    def __init__(self, id=None, product_name=None, price=None, company_name=None):
        self.id = id
        self.product_name = product_name
        self.price = price
        self.__price__ = True
        self.company_name = company_name

    def __repr__(self):
        return '<Price productname: ' + self.product_name.encode('utf-8') + '>'
