# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/company.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_CompanyDialog(object):
    def setupUi(self, CompanyDialog):
        CompanyDialog.setObjectName(_fromUtf8("CompanyDialog"))
        CompanyDialog.resize(566, 640)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/avatar.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        CompanyDialog.setWindowIcon(icon)
        CompanyDialog.setStyleSheet(_fromUtf8("QFrame#frame {\n"
"border: 2px solid;\n"
"    border-radius: 6px;\n"
"}"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(CompanyDialog)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.label = QtGui.QLabel(CompanyDialog)
        self.label.setMinimumSize(QtCore.QSize(0, 51))
        font = QtGui.QFont()
        font.setPointSize(17)
        self.label.setFont(font)
        self.label.setStyleSheet(_fromUtf8("border-radius: 6px;\n"
"border: 2px solid;"))
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout_3.addWidget(self.label)
        self.frame = QtGui.QFrame(CompanyDialog)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.frame)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_3.addWidget(self.label_2)
        self.leCompanyName = QtGui.QLineEdit(self.frame)
        self.leCompanyName.setObjectName(_fromUtf8("leCompanyName"))
        self.horizontalLayout_3.addWidget(self.leCompanyName)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.label_4 = QtGui.QLabel(self.frame)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout_2.addWidget(self.label_4)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.twExcelFiles = QtGui.QTableWidget(self.frame)
        self.twExcelFiles.setColumnCount(3)
        self.twExcelFiles.setObjectName(_fromUtf8("twExcelFiles"))
        self.twExcelFiles.setRowCount(0)
        self.verticalLayout.addWidget(self.twExcelFiles)
        spacerItem = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.pbAddExcelFile = QtGui.QPushButton(self.frame)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/add.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbAddExcelFile.setIcon(icon1)
        self.pbAddExcelFile.setObjectName(_fromUtf8("pbAddExcelFile"))
        self.horizontalLayout_2.addWidget(self.pbAddExcelFile)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.verticalLayout_3.addWidget(self.frame)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout_3.addItem(spacerItem2)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem3)
        self.pbCancel = QtGui.QPushButton(CompanyDialog)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/cancel-icon-47512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbCancel.setIcon(icon2)
        self.pbCancel.setObjectName(_fromUtf8("pbCancel"))
        self.horizontalLayout.addWidget(self.pbCancel)
        self.pbSave = QtGui.QPushButton(CompanyDialog)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/ok-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbSave.setIcon(icon3)
        self.pbSave.setObjectName(_fromUtf8("pbSave"))
        self.horizontalLayout.addWidget(self.pbSave)
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem4)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.label.raise_()
        self.frame.raise_()

        self.retranslateUi(CompanyDialog)
        QtCore.QMetaObject.connectSlotsByName(CompanyDialog)

    def retranslateUi(self, CompanyDialog):
        CompanyDialog.setWindowTitle(_translate("CompanyDialog", "Company", None))
        self.label.setText(_translate("CompanyDialog", "Company", None))
        self.label_2.setText(_translate("CompanyDialog", "Name:", None))
        self.label_4.setText(_translate("CompanyDialog", "Price Excel File(s)", None))
        self.pbAddExcelFile.setText(_translate("CompanyDialog", "Add Excel File", None))
        self.pbCancel.setText(_translate("CompanyDialog", "Cancel", None))
        self.pbSave.setText(_translate("CompanyDialog", "Save", None))

import pricelist_rc
