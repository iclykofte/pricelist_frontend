# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/computer_builder_dialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ComputerBuilderDialog(object):
    def setupUi(self, ComputerBuilderDialog):
        ComputerBuilderDialog.setObjectName(_fromUtf8("ComputerBuilderDialog"))
        ComputerBuilderDialog.resize(580, 625)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/avatar.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        ComputerBuilderDialog.setWindowIcon(icon)
        self.verticalLayout = QtGui.QVBoxLayout(ComputerBuilderDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(ComputerBuilderDialog)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.leName = QtGui.QLineEdit(ComputerBuilderDialog)
        self.leName.setObjectName(_fromUtf8("leName"))
        self.gridLayout.addWidget(self.leName, 0, 1, 1, 2)
        self.deDate = QtGui.QDateEdit(ComputerBuilderDialog)
        self.deDate.setObjectName(_fromUtf8("deDate"))
        self.gridLayout.addWidget(self.deDate, 1, 1, 1, 1)
        spacerItem = QtGui.QSpacerItem(158, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 1, 2, 1, 1)
        self.label_2 = QtGui.QLabel(ComputerBuilderDialog)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.label_3 = QtGui.QLabel(ComputerBuilderDialog)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout.addWidget(self.label_3)
        self.twComponents = QtGui.QTableWidget(ComputerBuilderDialog)
        self.twComponents.setColumnCount(3)
        self.twComponents.setObjectName(_fromUtf8("twComponents"))
        self.twComponents.setRowCount(0)
        self.verticalLayout.addWidget(self.twComponents)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.pbSave = QtGui.QPushButton(ComputerBuilderDialog)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/ok-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbSave.setIcon(icon1)
        self.pbSave.setObjectName(_fromUtf8("pbSave"))
        self.horizontalLayout_2.addWidget(self.pbSave)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.pbClearAll = QtGui.QPushButton(ComputerBuilderDialog)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/delete-icon.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbClearAll.setIcon(icon2)
        self.pbClearAll.setObjectName(_fromUtf8("pbClearAll"))
        self.horizontalLayout_2.addWidget(self.pbClearAll)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        spacerItem4 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem4)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem5 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem5)
        self.pbClose = QtGui.QPushButton(ComputerBuilderDialog)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/cancel-icon-47512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbClose.setIcon(icon3)
        self.pbClose.setObjectName(_fromUtf8("pbClose"))
        self.horizontalLayout.addWidget(self.pbClose)
        spacerItem6 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem6)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(ComputerBuilderDialog)
        QtCore.QMetaObject.connectSlotsByName(ComputerBuilderDialog)

    def retranslateUi(self, ComputerBuilderDialog):
        ComputerBuilderDialog.setWindowTitle(_translate("ComputerBuilderDialog", "Computer Builder", None))
        self.label.setText(_translate("ComputerBuilderDialog", "Name:", None))
        self.label_2.setText(_translate("ComputerBuilderDialog", "Date:", None))
        self.label_3.setText(_translate("ComputerBuilderDialog", "Components:", None))
        self.pbSave.setText(_translate("ComputerBuilderDialog", "Save", None))
        self.pbClearAll.setText(_translate("ComputerBuilderDialog", "Clear All", None))
        self.pbClose.setText(_translate("ComputerBuilderDialog", "Close", None))

import pricelist_rc
