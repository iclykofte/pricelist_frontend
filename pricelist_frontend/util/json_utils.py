from PyQt4 import QtCore
from json import JSONEncoder

from pricelist_frontend.model.company import Company
from pricelist_frontend.model.price import Price

from pricelist_frontend.model.price_list import PriceList


class CompanyEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o.__class__, QtCore.QString):
            return unicode(o).__dict__
        return o.__dict__


class PriceListEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o.__class__, QtCore.QString):
            return unicode(o).__dict__
        return o.__dict__


def company_decoder(obj):
    if '__company__' in obj:
        company = Company(id=obj['id'], name=obj['name'])
        company.price_lists = []
        for pl in obj['price_lists']:
            company.price_lists.append(pricelist_decoder(pl))

        return company

    return obj


def pricelist_decoder(obj):
    if '__pricelist__' in obj:
        return PriceList(id=obj['id'], filename=obj['filename'], products_row=obj['products_row'],
                         product_name_col=obj['product_name_col'], price_col=obj['price_col'], pageno=obj['pageno'])

    return obj


def price_decoder(obj):
    if '__price__' in obj:
        p = None
        try:
            price_obj = float(obj['price'])
        except:
            print('cannot convert to price_obj')

    return Price(id=obj['id'], product_name=obj['product_name'], price=price_obj)


def pricequery_decoder(obj):
    price_part = obj[0]
    price = price_decoder(price_part)
    price.company_name = obj[1]

    return price
