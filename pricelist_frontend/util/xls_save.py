import xlwt


def save_computer(c, filename, customer_name, date):
    workbook = xlwt.Workbook(encoding='utf-8')
    sheet = workbook.add_sheet('iclykofte')

    style = xlwt.easyxf('font: bold 1')
    # add customer name
    sheet.write(0, 0, 'Customer Name:', style)
    sheet.write(0, 1, unicode(customer_name))

    # add date
    sheet.write(1, 0, 'Date:', style)
    sheet.write(1, 1, unicode(date))

    # add header
    sheet.write(4, 0, 'Component Name', style)
    sheet.write(4, 1, 'Price', style)

    # add components
    row = 5
    for index, value in enumerate(c.components):
        sheet.write(row + index, 0, unicode(value.product_name))
        sheet.write(row + index, 1, unicode(value.price))

    sheet.write(row + len(c.components) + 3, 0, 'Total:', style)

    sheet.col(0).width = 256 * 100

    workbook.save(filename)
