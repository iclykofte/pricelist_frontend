from PyQt4 import QtCore
from PyQt4 import QtGui
from requests import HTTPError

from pricelist_frontend.company_dialog import CompanyDialog
from rest.company import *
from ui_company_list import Ui_CompanyListDialog
from util.json_utils import company_decoder
from util.restart import restart_program

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class CompanyListDialog(QtGui.QDialog, Ui_CompanyListDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

        self.twCompanies.verticalHeader().setDefaultSectionSize(56)

        self.connect(self.pbAddCompany, QtCore.SIGNAL("clicked()"), self.on_pbAddCompanyClicked)
        self.connect(self.pbOk, QtCore.SIGNAL('clicked()'), self.on_ok_clicked)
        self.connect(self.pbCancel, QtCore.SIGNAL('clicked()'), self.on_cancel_clicked)

        header = QtCore.QStringList()
        header.append('Name')
        header.append('Edit')
        header.append('Delete')
        self.twCompanies.setHorizontalHeaderLabels(header)
        h = self.twCompanies.horizontalHeader()
        h.setResizeMode(0, QtGui.QHeaderView.Stretch)
        h.setResizeMode(1, QtGui.QHeaderView.Interactive)

        self.editBtnList = []
        self.deleteBtnList = []
        self.company_list = []

        self.update_companies_gui()

    def on_ok_clicked(self):
        self.accept()

    def on_cancel_clicked(self):
        self.reject()

    def on_pbAddCompanyClicked(self):
        self.add_company_dialog = CompanyDialog()
        rv = self.add_company_dialog.exec_()

        if QtGui.QDialog.Accepted:
            print 'accepted!!!'
            self.update_companies_gui()

    def get_centered_cellbutton_widget(self, pb_icon):
        self.wedit = QtGui.QWidget()
        self.editlayout = QtGui.QHBoxLayout()
        self.editlayout.setAlignment(QtCore.Qt.AlignHCenter)
        self.pb = QtGui.QPushButton()
        self.pb.setFixedSize(QtCore.QSize(40, 40))
        self.pb.setIcon(QtGui.QIcon(_fromUtf8(pb_icon)))
        self.pb.setIconSize(QtCore.QSize(32, 32))
        self.editlayout.addWidget(self.pb)
        self.wedit.setLayout(self.editlayout)

        return self.wedit, self.pb

    def update_companies_gui(self):
        self.twCompanies.clearContents()
        self.editBtnList = []
        self.deleteBtnList = []
        self.company_list = []
        try:
            comp = get_companies()

            self.twCompanies.setRowCount(len(comp))
            for i, c in enumerate(comp):
                company = company_decoder(c)

                item = QtGui.QTableWidgetItem(company.name)
                self.twCompanies.setItem(i, 0, item)

                self.editWidget, self.pEdit = self.get_centered_cellbutton_widget(":/images/res/edit-notes.png")
                self.connect(self.pEdit, QtCore.SIGNAL('clicked()'), self.edit_Company)
                self.twCompanies.setCellWidget(i, 1, self.editWidget)

                self.deleteWidget, self.pDelete = self.get_centered_cellbutton_widget(":/images/res/delete-icon.png")
                self.connect(self.pDelete, QtCore.SIGNAL('clicked()'), self.delete_Company)
                self.twCompanies.setCellWidget(i, 2, self.deleteWidget)

                self.editBtnList.append(self.pEdit)
                self.deleteBtnList.append(self.pDelete)
                self.company_list.append(company)
        except HTTPError as e:
            if e.response.status_code == 401:
                QtGui.QMessageBox.warning(self, 'Error', 'Your session has timed out!')
                restart_program()



    def edit_Company(self):
        row = self.editBtnList.index(self.sender())

        self.edit_company_dialog = CompanyDialog()
        self.edit_company_dialog.set_company(self.company_list[row])
        rv = self.edit_company_dialog.exec_()

        if rv == QtGui.QDialog.Accepted:
            print 'accepted!!!'
            self.update_companies_gui()

    def delete_Company(self):
        row = self.deleteBtnList.index(self.sender())
        del_question = 'Are you sure to delete %s company' % self.company_list[row].name

        if QtGui.QMessageBox.question(self, 'Comfirmation', del_question,
                                      QtGui.QMessageBox.Yes, QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
            print 'deleting'
            try:
                if delete_company(self.company_list[row]) == True:
                    QtGui.QMessageBox.information(self, 'Success', 'Company successfully deleted!!!')
                    self.update_companies_gui()
                else:
                    QtGui.QMessageBox.warning(self, 'Error', 'Company can not be deleted!!!')
            except HTTPError as e:
                if e.response.status_code == 401:
                    QtGui.QMessageBox.warning(self, 'Error', 'Your session has timed out!')
                    restart_program()
