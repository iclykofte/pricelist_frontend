from PyQt4 import QtCore
from PyQt4 import QtGui
from ui_sales_dialog import Ui_SalesSettingsDialog


class SalesSettingsDialog(QtGui.QDialog, Ui_SalesSettingsDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

        self.connect(self.pbOk, QtCore.SIGNAL('clicked()'), self.on_pbok_clicked)
        self.connect(self.pbCancel, QtCore.SIGNAL('clicked()'), self.on_pbcancel_clicked)

        self.settings = QtCore.QSettings('Casper', 'pricelist')

        self.sbSalesPercentage.setValue(self.settings.value('sales/percent', 20).toInt()[0])
        self.leUsd.setText(self.settings.value('sales/usd', 335).toString())

    def on_pbok_clicked(self):
        self.settings.setValue('sales/percent', self.sbSalesPercentage.value())
        self.settings.setValue('sales/usd', self.leUsd.text())
        self.accept()

    def on_pbcancel_clicked(self):
        self.reject()
