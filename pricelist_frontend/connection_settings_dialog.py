from PyQt4 import QtGui
from PyQt4 import QtCore
from ui_connection_settings_dialog import Ui_ConnectionSettingsDialog
from rest.connection_info import connection


class ConnectionSettingsDialog(QtGui.QDialog, Ui_ConnectionSettingsDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

        self.settings = QtCore.QSettings('Casper', 'pricelist')

        self.host_endpoint = unicode(self.settings.value('connection/endpoint', 'http://127.0.0.1:8080').toString())
        self.leHostEndpoint.setText(self.host_endpoint)

        self.connect(self.pbOk, QtCore.SIGNAL('clicked()'), self.on_pbok_clicked)
        self.connect(self.pbCancel, QtCore.SIGNAL('clicked()'), self.on_pbcancel_clicked)

    def on_pbok_clicked(self):
        host_endpoint = unicode(self.leHostEndpoint.text())
        if host_endpoint == '' or host_endpoint == '':
            return

        self.settings.setValue('connection/endpoint', host_endpoint)
        connection.endpoint = host_endpoint
        self.accept()

    def on_pbcancel_clicked(self):
        self.reject()
