# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/mainwindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(824, 666)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        MainWindow.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/avatar.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet(_fromUtf8("QMainWindow {\n"
"background-color: white;\n"
"}\n"
"\n"
"QListView, QLineEdit {\n"
"    color: rgb(127, 0, 63);\n"
"    selection-color: white;   \n"
"    border: 2px groove gray;\n"
"    border-radius: 10px;\n"
"    padding: 2px 4px;\n"
"}\n"
"QLineEdit:focus {\n"
"    color: rgb(127, 0, 63);\n"
"    selection-color: white;   \n"
"    border: 2px groove gray;\n"
"    border-radius: 10px;\n"
"    padding: 2px 4px;\n"
"}\n"
"\n"
"QLineEdit:edit-focus {\n"
"    color: rgb(127, 0, 63);\n"
"    selection-color: white;   \n"
"    border: 2px groove gray;\n"
"    border-radius: 10px;\n"
"    padding: 2px 4px;\n"
"}"))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.wBanner = QtGui.QWidget(self.centralwidget)
        self.wBanner.setObjectName(_fromUtf8("wBanner"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.wBanner)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.widget = QtGui.QWidget(self.wBanner)
        self.widget.setMinimumSize(QtCore.QSize(660, 220))
        self.widget.setMaximumSize(QtCore.QSize(660, 220))
        self.widget.setStyleSheet(_fromUtf8("background-image: url(:/images/res/bg5.png);\n"
"border: 3px solid white;\n"
"border-radius: 10px;\n"
""))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.horizontalLayout_2.addWidget(self.widget)
        self.verticalLayout.addWidget(self.wBanner)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.leSearch = QtGui.QLineEdit(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.leSearch.sizePolicy().hasHeightForWidth())
        self.leSearch.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.leSearch.setFont(font)
        self.leSearch.setObjectName(_fromUtf8("leSearch"))
        self.horizontalLayout.addWidget(self.leSearch)
        self.pbSearch = QtGui.QPushButton(self.centralwidget)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/search.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbSearch.setIcon(icon1)
        self.pbSearch.setIconSize(QtCore.QSize(60, 60))
        self.pbSearch.setObjectName(_fromUtf8("pbSearch"))
        self.horizontalLayout.addWidget(self.pbSearch)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.twResults = QtGui.QTableWidget(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(2)
        sizePolicy.setHeightForWidth(self.twResults.sizePolicy().hasHeightForWidth())
        self.twResults.setSizePolicy(sizePolicy)
        self.twResults.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.twResults.setColumnCount(6)
        self.twResults.setObjectName(_fromUtf8("twResults"))
        self.twResults.setRowCount(0)
        self.verticalLayout.addWidget(self.twResults)
        spacerItem2 = QtGui.QSpacerItem(20, 47, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 824, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        self.menuCompany = QtGui.QMenu(self.menubar)
        self.menuCompany.setObjectName(_fromUtf8("menuCompany"))
        self.menuComputer_Builder = QtGui.QMenu(self.menubar)
        self.menuComputer_Builder.setObjectName(_fromUtf8("menuComputer_Builder"))
        self.menuSettings = QtGui.QMenu(self.menubar)
        self.menuSettings.setObjectName(_fromUtf8("menuSettings"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionQuit = QtGui.QAction(MainWindow)
        self.actionQuit.setObjectName(_fromUtf8("actionQuit"))
        self.actionAddCompany = QtGui.QAction(MainWindow)
        self.actionAddCompany.setObjectName(_fromUtf8("actionAddCompany"))
        self.actionListCompany = QtGui.QAction(MainWindow)
        self.actionListCompany.setObjectName(_fromUtf8("actionListCompany"))
        self.actionBuildComputer = QtGui.QAction(MainWindow)
        self.actionBuildComputer.setObjectName(_fromUtf8("actionBuildComputer"))
        self.actionRebuild_Prices = QtGui.QAction(MainWindow)
        self.actionRebuild_Prices.setObjectName(_fromUtf8("actionRebuild_Prices"))
        self.actionSales_Settings = QtGui.QAction(MainWindow)
        self.actionSales_Settings.setObjectName(_fromUtf8("actionSales_Settings"))
        self.actionChange_Password = QtGui.QAction(MainWindow)
        self.actionChange_Password.setObjectName(_fromUtf8("actionChange_Password"))
        self.actionConnection_Settings = QtGui.QAction(MainWindow)
        self.actionConnection_Settings.setObjectName(_fromUtf8("actionConnection_Settings"))
        self.menuFile.addAction(self.actionRebuild_Prices)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionQuit)
        self.menuCompany.addAction(self.actionAddCompany)
        self.menuCompany.addAction(self.actionListCompany)
        self.menuComputer_Builder.addAction(self.actionBuildComputer)
        self.menuSettings.addAction(self.actionSales_Settings)
        self.menuSettings.addAction(self.actionConnection_Settings)
        self.menuSettings.addSeparator()
        self.menuSettings.addAction(self.actionChange_Password)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuCompany.menuAction())
        self.menubar.addAction(self.menuComputer_Builder.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Price List", None))
        self.leSearch.setPlaceholderText(_translate("MainWindow", "Type product information to search...", None))
        self.pbSearch.setText(_translate("MainWindow", "Search", None))
        self.menuFile.setTitle(_translate("MainWindow", "File", None))
        self.menuCompany.setTitle(_translate("MainWindow", "Company", None))
        self.menuComputer_Builder.setTitle(_translate("MainWindow", "Computer Builder", None))
        self.menuSettings.setTitle(_translate("MainWindow", "Settings", None))
        self.actionQuit.setText(_translate("MainWindow", "Quit", None))
        self.actionAddCompany.setText(_translate("MainWindow", "Add", None))
        self.actionListCompany.setText(_translate("MainWindow", "List", None))
        self.actionBuildComputer.setText(_translate("MainWindow", "Build", None))
        self.actionRebuild_Prices.setText(_translate("MainWindow", "Rebuild Prices", None))
        self.actionSales_Settings.setText(_translate("MainWindow", "Sales Settings", None))
        self.actionChange_Password.setText(_translate("MainWindow", "Change Password", None))
        self.actionConnection_Settings.setText(_translate("MainWindow", "Connection Settings", None))

import pricelist_rc
