from requests import HTTPError

from ui_company import Ui_CompanyDialog
from PyQt4 import QtGui
from PyQt4 import QtCore
from model.company import Company
from rest.company import *
from pricelist_dialog import PriceListDialog
from util.restart import restart_program

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class CompanyDialog(QtGui.QDialog, Ui_CompanyDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

        self.twExcelFiles.verticalHeader().setDefaultSectionSize(56);

        self.company = Company()

        header = QtCore.QStringList()
        header.append('Price List')
        header.append('Edit')
        header.append('Delete')
        self.twExcelFiles.setHorizontalHeaderLabels(header)

        h = self.twExcelFiles.horizontalHeader()
        h.setResizeMode(0, QtGui.QHeaderView.Stretch)
        h.setResizeMode(1, QtGui.QHeaderView.Interactive)

        self.connect(self.pbSave, QtCore.SIGNAL('clicked()'), self.on_save_clicked)
        self.connect(self.pbCancel, QtCore.SIGNAL('clicked()'), self.on_cancel_clicked)
        self.connect(self.pbAddExcelFile, QtCore.SIGNAL('clicked()'), self.on_addexcel_clicked)

        self.editBtnList = []
        self.deleteBtnList = []

        self.excels = []

    def set_company(self, company):
        self.company = company
        self.excels.extend(company.price_lists)

        self.leCompanyName.setText(self.company.name)

        self.update_pricelists_gui()

    def on_save_clicked(self):
        self.company.name = unicode(self.leCompanyName.text())
        self.company.price_lists = self.excels
        # add new company
        if self.company.id == None:
            try:
                if add_company(self.company) == True:
                    QtGui.QMessageBox.information(self, 'Information', 'Company successfully added.')
                else:
                    QtGui.QMessageBox.warning(self, 'Error', 'Company can not be added!')
            except HTTPError as e:
                if e.response.status_code == 401:
                    QtGui.QMessageBox.warning(self, 'Error', 'Your session has timed out!')
                    restart_program()
        else:  # update
            try:
                if update_company(self.company) == True:
                    QtGui.QMessageBox.information(self, 'Information', 'Company updated.')
                else:
                    QtGui.QMessageBox.warning(self, 'Error', 'Company can not be updated!')
            except HTTPError as e:
                if e.response.status_code == 401:
                    QtGui.QMessageBox.warning(self, 'Error', 'Your session has timed out!')
                    restart_program()

        self.accept()

    def on_cancel_clicked(self):
        self.reject()

    def on_addexcel_clicked(self):
        self.addexcel = PriceListDialog()
        if self.addexcel.exec_() == QtGui.QDialog.Accepted:
            self.excels.append(self.addexcel.pricelist)
            self.update_pricelists_gui()

    def get_centered_cellbutton_widget(self, pb_icon):
        self.wedit = QtGui.QWidget()
        self.editlayout = QtGui.QHBoxLayout()
        self.editlayout.setAlignment(QtCore.Qt.AlignHCenter)
        self.pb = QtGui.QPushButton()
        self.pb.setFixedSize(QtCore.QSize(40, 40))
        self.pb.setIcon(QtGui.QIcon(_fromUtf8(pb_icon)))
        self.pb.setIconSize(QtCore.QSize(32, 32))
        self.editlayout.addWidget(self.pb)
        self.wedit.setLayout(self.editlayout)

        return self.wedit, self.pb

    def update_pricelists_gui(self):
        self.twExcelFiles.clearContents()
        self.editBtnList = []
        self.deleteBtnList = []

        self.twExcelFiles.setRowCount(len(self.excels))
        for i, excel in enumerate(self.excels):
            item = QtGui.QTableWidgetItem(excel.filename)
            self.twExcelFiles.setItem(i, 0, item)

            self.editWidget, self.pEdit = self.get_centered_cellbutton_widget(":/images/res/edit-notes.png")
            self.connect(self.pEdit, QtCore.SIGNAL('clicked()'), self.edit_excel)
            self.twExcelFiles.setCellWidget(i, 1, self.editWidget)

            self.deleteWidget, self.pDelete = self.get_centered_cellbutton_widget(":/images/res/delete-icon.png")
            self.connect(self.pDelete, QtCore.SIGNAL('clicked()'), self.delete_excel)
            self.twExcelFiles.setCellWidget(i, 2, self.deleteWidget)

            self.editBtnList.append(self.pEdit)
            self.deleteBtnList.append(self.pDelete)

    def edit_excel(self):
        row = self.editBtnList.index(self.sender())

        self.pricelist_dialog = PriceListDialog()
        self.pricelist_dialog.set_pricelist(self.excels[row])
        rv = self.pricelist_dialog.exec_()

        if rv == QtGui.QDialog.Accepted:
            self.excels[row] = self.pricelist_dialog.pricelist
            self.update_pricelists_gui()

    def delete_excel(self):
        row = self.deleteBtnList.index(self.sender())
        del_question = 'Are you sure to delete %s price list' % self.excels[row].filename

        if QtGui.QMessageBox.question(self, 'Comfirmation', del_question,
                                      QtGui.QMessageBox.Yes, QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
            self.excels.remove(self.excels[row])
            self.update_pricelists_gui()
