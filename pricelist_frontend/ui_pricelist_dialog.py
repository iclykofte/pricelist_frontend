# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/pricelist_dialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_PriceListlDialog(object):
    def setupUi(self, PriceListlDialog):
        PriceListlDialog.setObjectName(_fromUtf8("PriceListlDialog"))
        PriceListlDialog.resize(596, 281)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/avatar.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        PriceListlDialog.setWindowIcon(icon)
        PriceListlDialog.setStyleSheet(_fromUtf8(""))
        self.verticalLayout_2 = QtGui.QVBoxLayout(PriceListlDialog)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.frame = QtGui.QFrame(PriceListlDialog)
        self.frame.setStyleSheet(_fromUtf8("QFrame#frame {\n"
"border: 2px solid;\n"
"    border-radius: 6px;\n"
"}"))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.verticalLayout = QtGui.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setMaximumSize(QtCore.QSize(107, 16777215))
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.pbChooseExcelFile = QtGui.QPushButton(self.frame)
        self.pbChooseExcelFile.setMaximumSize(QtCore.QSize(31, 16777215))
        self.pbChooseExcelFile.setObjectName(_fromUtf8("pbChooseExcelFile"))
        self.horizontalLayout.addWidget(self.pbChooseExcelFile)
        self.lblExcelFile = QtGui.QLabel(self.frame)
        self.lblExcelFile.setText(_fromUtf8(""))
        self.lblExcelFile.setObjectName(_fromUtf8("lblExcelFile"))
        self.horizontalLayout.addWidget(self.lblExcelFile)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_5 = QtGui.QLabel(self.frame)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.horizontalLayout_4.addWidget(self.label_5)
        self.sbProductsRow = QtGui.QSpinBox(self.frame)
        self.sbProductsRow.setMinimum(1)
        self.sbProductsRow.setProperty("value", 1)
        self.sbProductsRow.setObjectName(_fromUtf8("sbProductsRow"))
        self.horizontalLayout_4.addWidget(self.sbProductsRow)
        spacerItem1 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_5.addWidget(self.label_2)
        self.leNameColumn = QtGui.QLineEdit(self.frame)
        self.leNameColumn.setObjectName(_fromUtf8("leNameColumn"))
        self.horizontalLayout_5.addWidget(self.leNameColumn)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem2)
        self.label_3 = QtGui.QLabel(self.frame)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_5.addWidget(self.label_3)
        self.lePriceColumn = QtGui.QLineEdit(self.frame)
        self.lePriceColumn.setObjectName(_fromUtf8("lePriceColumn"))
        self.horizontalLayout_5.addWidget(self.lePriceColumn)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_4 = QtGui.QLabel(self.frame)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_3.addWidget(self.label_4)
        self.sbPageNo = QtGui.QSpinBox(self.frame)
        self.sbPageNo.setMinimum(1)
        self.sbPageNo.setObjectName(_fromUtf8("sbPageNo"))
        self.horizontalLayout_3.addWidget(self.sbPageNo)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.verticalLayout_2.addWidget(self.frame)
        spacerItem4 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout_2.addItem(spacerItem4)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem5 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem5)
        self.pbCancel = QtGui.QPushButton(PriceListlDialog)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/cancel-icon-47512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbCancel.setIcon(icon1)
        self.pbCancel.setObjectName(_fromUtf8("pbCancel"))
        self.horizontalLayout_2.addWidget(self.pbCancel)
        self.pbOk = QtGui.QPushButton(PriceListlDialog)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/ok-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbOk.setIcon(icon2)
        self.pbOk.setObjectName(_fromUtf8("pbOk"))
        self.horizontalLayout_2.addWidget(self.pbOk)
        spacerItem6 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem6)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        self.retranslateUi(PriceListlDialog)
        QtCore.QMetaObject.connectSlotsByName(PriceListlDialog)

    def retranslateUi(self, PriceListlDialog):
        PriceListlDialog.setWindowTitle(_translate("PriceListlDialog", "Price Excel Configuration", None))
        self.label.setText(_translate("PriceListlDialog", "Select price list:", None))
        self.pbChooseExcelFile.setText(_translate("PriceListlDialog", "...", None))
        self.label_5.setText(_translate("PriceListlDialog", "Products Starting Row:", None))
        self.label_2.setText(_translate("PriceListlDialog", "Product Name Column:", None))
        self.leNameColumn.setText(_translate("PriceListlDialog", "A", None))
        self.label_3.setText(_translate("PriceListlDialog", "Price Column:", None))
        self.lePriceColumn.setText(_translate("PriceListlDialog", "B", None))
        self.label_4.setText(_translate("PriceListlDialog", "Page #:", None))
        self.pbCancel.setText(_translate("PriceListlDialog", "Cancel", None))
        self.pbOk.setText(_translate("PriceListlDialog", "OK", None))

import pricelist_rc
