# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/company_list.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_CompanyListDialog(object):
    def setupUi(self, CompanyListDialog):
        CompanyListDialog.setObjectName(_fromUtf8("CompanyListDialog"))
        CompanyListDialog.resize(622, 602)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/avatar.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        CompanyListDialog.setWindowIcon(icon)
        CompanyListDialog.setStyleSheet(_fromUtf8(""))
        self.verticalLayout = QtGui.QVBoxLayout(CompanyListDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(CompanyListDialog)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label.setFont(font)
        self.label.setStyleSheet(_fromUtf8("border: 2px solid;\n"
"    border-radius: 6px;"))
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.twCompanies = QtGui.QTableWidget(CompanyListDialog)
        self.twCompanies.setStyleSheet(_fromUtf8(""))
        self.twCompanies.setAlternatingRowColors(True)
        self.twCompanies.setColumnCount(3)
        self.twCompanies.setObjectName(_fromUtf8("twCompanies"))
        self.twCompanies.setRowCount(0)
        self.verticalLayout.addWidget(self.twCompanies)
        spacerItem = QtGui.QSpacerItem(20, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.pbAddCompany = QtGui.QPushButton(CompanyListDialog)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/add.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbAddCompany.setIcon(icon1)
        self.pbAddCompany.setObjectName(_fromUtf8("pbAddCompany"))
        self.horizontalLayout_2.addWidget(self.pbAddCompany)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem2)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem3)
        self.pbCancel = QtGui.QPushButton(CompanyListDialog)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/cancel-icon-47512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbCancel.setIcon(icon2)
        self.pbCancel.setObjectName(_fromUtf8("pbCancel"))
        self.horizontalLayout.addWidget(self.pbCancel)
        self.pbOk = QtGui.QPushButton(CompanyListDialog)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/ok-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbOk.setIcon(icon3)
        self.pbOk.setObjectName(_fromUtf8("pbOk"))
        self.horizontalLayout.addWidget(self.pbOk)
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem4)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(CompanyListDialog)
        QtCore.QMetaObject.connectSlotsByName(CompanyListDialog)

    def retranslateUi(self, CompanyListDialog):
        CompanyListDialog.setWindowTitle(_translate("CompanyListDialog", "Company List", None))
        self.label.setText(_translate("CompanyListDialog", "Companies", None))
        self.pbAddCompany.setText(_translate("CompanyListDialog", "Add Company", None))
        self.pbCancel.setText(_translate("CompanyListDialog", "Cancel", None))
        self.pbOk.setText(_translate("CompanyListDialog", "OK", None))

import pricelist_rc
