import sys
from PyQt4 import QtGui
from mainwindow import PriceListMainWindow
from login import LoginDialog
import logging

logging.basicConfig(filename='pricelistgui.log',level=logging.DEBUG)

logging.debug('Starting Pricelist GUI application')


def main(args=None):
    app = QtGui.QApplication(sys.argv)

    login_dialog = LoginDialog()
    if login_dialog.exec_() == QtGui.QDialog.Accepted:
        w = PriceListMainWindow()
        w.showMaximized()
    else:
        sys.exit(0)

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
