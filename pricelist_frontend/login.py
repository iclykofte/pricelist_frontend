from PyQt4 import QtGui
from PyQt4 import QtCore

from connection_settings_dialog import ConnectionSettingsDialog
from ui_login import Ui_Login
from rest.login import get_auth_token
from pricelist_frontend.model.user import user
from requests.exceptions import HTTPError, ConnectTimeout
from rest.connection_info import connection


class LoginDialog(QtGui.QDialog, Ui_Login):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

        self.settings = QtCore.QSettings('Casper', 'pricelist')

        connection.endpoint = unicode(self.settings.value('connection/endpoint', 'http://127.0.0.1:8080').toString())

        self.user = None

        self.connect(self.pbOk, QtCore.SIGNAL('clicked()'), self.on_pbok_clicked)
        self.connect(self.pbCancel, QtCore.SIGNAL('clicked()'), self.on_pbcancel_clicked)
        self.connect(self.pbConnectionSettings, QtCore.SIGNAL('clicked()'), self.on_pbconnection_settings_clicked)

    def set_user(self, u):
        self.user = u
        self.leUsername.setText(self.user.username)
        self.leUsername.setEnabled(False)

    def on_pbconnection_settings_clicked(self):
        self.conn_settings_dlg = ConnectionSettingsDialog()
        if self.conn_settings_dlg.exec_() == QtGui.QDialog.Accepted:
            QtGui.QMessageBox.information(self, 'Success', 'Endpoind succesfully changed.')

    def on_pbok_clicked(self):
        usr = unicode(self.leUsername.text())
        passwd = unicode(self.lePassword.text())
        if usr == '' or passwd == '':
            return

        if len(passwd) < 6:
            QtGui.QMessageBox.warning(self, 'Warning', 'Password must be 6 characters or more!')
            return

        if self.user:  # update
            self.accept()
        else:  # login
            try:
                token = get_auth_token(usr, passwd)
                # store for rest calls
                user.auth_token = token['token']
                user.username = usr
                self.accept()
            except HTTPError as err:
                if err.response.status_code == 401:
                    QtGui.QMessageBox.warning(self, 'Error', 'Invalid username or password!')
            except ConnectTimeout:
                QtGui.QMessageBox.warning(self, 'Error',
                                          'Unable to connect endpoint, check endpoint settings and server!')

    def on_pbcancel_clicked(self):
        self.reject()


