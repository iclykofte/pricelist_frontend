# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/connection_settings_dialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ConnectionSettingsDialog(object):
    def setupUi(self, ConnectionSettingsDialog):
        ConnectionSettingsDialog.setObjectName(_fromUtf8("ConnectionSettingsDialog"))
        ConnectionSettingsDialog.resize(530, 123)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/avatar.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        ConnectionSettingsDialog.setWindowIcon(icon)
        self.verticalLayout = QtGui.QVBoxLayout(ConnectionSettingsDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label = QtGui.QLabel(ConnectionSettingsDialog)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_2.addWidget(self.label)
        self.leHostEndpoint = QtGui.QLineEdit(ConnectionSettingsDialog)
        self.leHostEndpoint.setObjectName(_fromUtf8("leHostEndpoint"))
        self.horizontalLayout_2.addWidget(self.leHostEndpoint)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        spacerItem = QtGui.QSpacerItem(20, 32, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.pbCancel = QtGui.QPushButton(ConnectionSettingsDialog)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/cancel-icon-47512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbCancel.setIcon(icon1)
        self.pbCancel.setDefault(True)
        self.pbCancel.setObjectName(_fromUtf8("pbCancel"))
        self.horizontalLayout.addWidget(self.pbCancel)
        self.pbOk = QtGui.QPushButton(ConnectionSettingsDialog)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/ok-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbOk.setIcon(icon2)
        self.pbOk.setDefault(False)
        self.pbOk.setObjectName(_fromUtf8("pbOk"))
        self.horizontalLayout.addWidget(self.pbOk)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(ConnectionSettingsDialog)
        QtCore.QMetaObject.connectSlotsByName(ConnectionSettingsDialog)

    def retranslateUi(self, ConnectionSettingsDialog):
        ConnectionSettingsDialog.setWindowTitle(_translate("ConnectionSettingsDialog", "Connection Settings", None))
        self.label.setToolTip(_translate("ConnectionSettingsDialog", "<html><head/><body><p>API Endpoint is typically your RESTful service url. For example if your price list backend application runs on the same host it will be:</p><p>http://127.0.0.1:8080</p><p>which means it speaks http (not https), and on IP address 127.0.0.1 (localhost) and port 8080</p></body></html>", None))
        self.label.setText(_translate("ConnectionSettingsDialog", "API Endpoint:", None))
        self.leHostEndpoint.setToolTip(_translate("ConnectionSettingsDialog", "<html><head/><body><p>API Endpoint is typically your RESTful service url. For example if your price list backend application runs on the same host it will be:</p><p>http://127.0.0.1:8080</p><p>which means it speaks http (not https), and on IP address 127.0.0.1 (localhost) and port 8080</p></body></html>", None))
        self.pbCancel.setText(_translate("ConnectionSettingsDialog", "Cancel", None))
        self.pbOk.setText(_translate("ConnectionSettingsDialog", "OK", None))

import pricelist_rc
