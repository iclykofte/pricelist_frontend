from PyQt4 import QtCore
from PyQt4 import QtGui
from requests import HTTPError

from pricelist_frontend.company_dialog import CompanyDialog
from pricelist_frontend.company_list import CompanyListDialog
from computer_builder_dialog import ComputerBuilderDialog
from connection_settings_dialog import ConnectionSettingsDialog
from login import LoginDialog
from model.computer import computer
from model.price import Price
from model.user import user
from pricelist_frontend.util.restart import restart_program
from rest.company import get_companies
from rest.connection_info import connection
from rest.login import change_password
from rest.prices import get_matched_products
from rest.prices import update_pricelist
from sales_dialog import SalesSettingsDialog
from ui_mainwindow import Ui_MainWindow
from util.json_utils import company_decoder, pricequery_decoder
from progresswindow import ProgressWindow


import logging

import os
from errno import ENOENT


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class PriceListMainWindow(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setupUi(self)

        self.twResults.hide()

        header = QtCore.QStringList()
        header.append('Name')
        header.append('Company')
        header.append('Price')
        header.append('Sale Price')
        header.append('Local Sale Price')
        header.append('Add')
        self.twResults.setHorizontalHeaderLabels(header)
        h = self.twResults.horizontalHeader()
        h.setResizeMode(0, QtGui.QHeaderView.Stretch)
        h.setResizeMode(1, QtGui.QHeaderView.Interactive)
        self.twResults.verticalHeader().setDefaultSectionSize(56)

        self.connect(self.actionAddCompany, QtCore.SIGNAL("triggered()"), self.on_actionAddCompany)
        self.connect(self.actionConnection_Settings, QtCore.SIGNAL("triggered()"),
                     self.on_pbconnection_settings_clicked)
        self.connect(self.actionListCompany, QtCore.SIGNAL("triggered()"), self.on_actionListCompanies)
        self.connect(self.actionQuit, QtCore.SIGNAL("triggered()"), QtCore.QCoreApplication.instance().quit)
        self.connect(self.actionSales_Settings, QtCore.SIGNAL("triggered()"), self.on_on_actionSalesSettings)
        self.connect(self.actionRebuild_Prices, QtCore.SIGNAL("triggered()"), self.on_actionRebuild_Prices)
        self.connect(self.actionBuildComputer, QtCore.SIGNAL("triggered()"), self.on_actionBuild_Computer)
        self.connect(self.leSearch, QtCore.SIGNAL("textChanged(const QString&)"), self.on_search_changed)
        self.connect(self.leSearch, QtCore.SIGNAL("returnPressed()"), self.on_search_init)
        self.connect(self.pbSearch, QtCore.SIGNAL("clicked()"), self.on_search_init)
        self.connect(self.actionChange_Password, QtCore.SIGNAL("triggered()"), self.on_change_password)

        self.settings = QtCore.QSettings('Casper', 'pricelist')

        self.salesPercentage = self.settings.value('sales/percent', 20).toInt()[0]
        self.usd = self.settings.value('sales/usd', 335).toDouble()[0]

        self.addBtnList = []

        self.search_terms = []

    def on_pbconnection_settings_clicked(self):
        self.conn_settings_dlg = ConnectionSettingsDialog()
        if self.conn_settings_dlg.exec_() == QtGui.QDialog.Accepted:
            connection.endpoint = self.conn_settings_dlg.host_endpoint

    def on_change_password(self):
        self.change_password_dialog = LoginDialog()
        self.change_password_dialog.set_user(user)
        if self.change_password_dialog.exec_() == QtGui.QDialog.Accepted:
            try:
                if change_password(user.username, unicode(self.change_password_dialog.lePassword.text())):
                    QtGui.QMessageBox.information(self, 'Password changed', 'Password succesfully changed.')
            except HTTPError as e:
                if e.response.status_code == 401:
                    QtGui.QMessageBox.warning(self, 'Error', 'Your session has timed out!')
                    restart_program()

    def get_centered_cellbutton_widget(self, pb_icon):
        self.wedit = QtGui.QWidget()
        self.buttonlayout = QtGui.QHBoxLayout()
        self.buttonlayout.setAlignment(QtCore.Qt.AlignHCenter)
        self.pb = QtGui.QPushButton()
        self.pb.setFixedSize(QtCore.QSize(40, 40))
        self.pb.setIcon(QtGui.QIcon(_fromUtf8(pb_icon)))
        self.pb.setIconSize(QtCore.QSize(32, 32))
        self.buttonlayout.addWidget(self.pb)
        self.wedit.setLayout(self.buttonlayout)

        return self.wedit, self.pb

    def on_actionAddCompany(self):
        self.company_dialog = CompanyDialog()
        self.company_dialog.exec_()

    def on_actionBuild_Computer(self):
        self.build_computer_dialog = ComputerBuilderDialog()
        self.build_computer_dialog.exec_()

    def on_actionListCompanies(self):
        self.listCompanies = CompanyListDialog()
        self.listCompanies.exec_()

    def on_on_actionSalesSettings(self):
        self.sales_settings_dialog = SalesSettingsDialog()
        if self.sales_settings_dialog.exec_() == QtGui.QDialog.Accepted:
            self.salesPercentage = self.settings.value('sales/percent', 20).toInt()[0]
            self.usd = self.settings.value('sales/usd', 335).toDouble()[0]
            self.update_search_results_gui()

    def on_actionRebuild_Prices(self):
        try:
            rest_companies = get_companies()
            self.company_list = [company_decoder(c) for c in rest_companies]
        except HTTPError as e:
            if e.response.status_code == 401:
                QtGui.QMessageBox.warning(self, 'Error', 'Your session has timed out!')
                restart_program()
        except Exception as e:
            QtGui.QMessageBox.information(self, 'Error', e.message)

        self.pw = ProgressWindow()
        self.pw.setProgressMax(len(self.company_list))
        self.pw.setHeader('Processing price list excel file:')

        self.pw.show()

        for current_index, c in enumerate(self.company_list):
            for p in c.price_lists:
                excel_filename = p.filename

                self.pw.setDesc(excel_filename)
                self.pw.setProgressCurrent(current_index)

                try:
                    logging.info('Processing price excel file %s' % p.filename)
                    p.get_prices()
                    update_pricelist(p)
                except IOError as e:
                    if e.errno == ENOENT:
                        logging.warning('Price List Excel File Not Found! Check pricelist excel file: %s' % e.filename)
                        QtGui.QMessageBox.information(self, 'Price List Excel File Not Found!',
                                              'Check pricelist excel file: %s' % e.filename)
                except Exception as e:
                    logging.warning('Excel file: %s, Error description: %s' % (excel_filename,str(e)))
                    QtGui.QMessageBox.information(self, 'Error while processing excel file',
                                                'Excel file: %s, Error description: %s' % (excel_filename, str(e)))

        self.pw.hide()


    def build_search_terms(self):
        self.search_terms = []
        entry = unicode(self.leSearch.text())
        for t in entry.split():
            if len(t) > 1 and t != None and t != '':
                self.search_terms.append(t)

    def on_search_changed(self, text):
        self.build_search_terms()

        if len(self.search_terms) > 0:
            self.twResults.show()
            self.wBanner.hide()
            self.update_search_results_gui()
        else:
            self.twResults.hide()
            self.wBanner.show()

    def on_search_init(self):
        self.build_search_terms()

        if len(self.search_terms) > 0:
            self.twResults.show()
            self.wBanner.hide()
            self.update_search_results_gui()
        else:
            self.twResults.hide()
            self.wBanner.show()

    def update_search_results_gui(self):
        self.addBtnList = []
        t = ' '.join(self.search_terms)
        self.twResults.clearContents()

        try:
            filtered_products = get_matched_products(unicode(t))
            self.twResults.setRowCount(len(filtered_products))
            for i, p in enumerate(filtered_products):
                product = pricequery_decoder(p)

                item = QtGui.QTableWidgetItem(product.product_name)
                self.twResults.setItem(i, 0, item)

                item = QtGui.QTableWidgetItem(product.company_name)
                self.twResults.setItem(i, 1, item)

                item = QtGui.QTableWidgetItem(str(product.price))
                self.twResults.setItem(i, 2, item)

                sales_dollar = product.price + product.price * self.salesPercentage / 100.
                item = QtGui.QTableWidgetItem(str(sales_dollar))
                self.twResults.setItem(i, 3, item)

                sales_local = self.usd * (product.price + product.price * self.salesPercentage / 100.)
                item = QtGui.QTableWidgetItem(str(sales_local))
                self.twResults.setItem(i, 4, item)

                self.addWidget, self.pAdd = self.get_centered_cellbutton_widget(":/images/res/add.png")
                self.connect(self.pAdd, QtCore.SIGNAL('clicked()'), self.add_component)
                self.twResults.setCellWidget(i, 5, self.addWidget)

                self.addBtnList.append(self.pAdd)
        except HTTPError as e:
            if e.response.status_code == 401:
                QtGui.QMessageBox.warning(self, 'Error', 'Your session has timed out!')
                restart_program()

    def add_component(self):
        row = self.addBtnList.index(self.sender())

        price = Price(product_name=self.twResults.item(row, 0).text(),
                      price=self.twResults.item(row, 2).text(),
                      company_name=self.twResults.item(row, 1).text())

        computer.add_component(price=price)
