# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/login.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Login(object):
    def setupUi(self, Login):
        Login.setObjectName(_fromUtf8("Login"))
        Login.resize(389, 293)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/avatar.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Login.setWindowIcon(icon)
        self.verticalLayout = QtGui.QVBoxLayout(Login)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.widget = QtGui.QWidget(Login)
        self.widget.setMinimumSize(QtCore.QSize(371, 141))
        self.widget.setMaximumSize(QtCore.QSize(371, 141))
        self.widget.setStyleSheet(_fromUtf8("background-image: url(:/images/res/login.png);"))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.verticalLayout.addWidget(self.widget)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(Login)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.leUsername = QtGui.QLineEdit(Login)
        self.leUsername.setMaxLength(50)
        self.leUsername.setObjectName(_fromUtf8("leUsername"))
        self.gridLayout.addWidget(self.leUsername, 0, 1, 1, 1)
        self.label_2 = QtGui.QLabel(Login)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.lePassword = QtGui.QLineEdit(Login)
        self.lePassword.setMaxLength(16)
        self.lePassword.setEchoMode(QtGui.QLineEdit.Password)
        self.lePassword.setObjectName(_fromUtf8("lePassword"))
        self.gridLayout.addWidget(self.lePassword, 1, 1, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem2 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem2)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.pbConnectionSettings = QtGui.QPushButton(Login)
        self.pbConnectionSettings.setMaximumSize(QtCore.QSize(50, 16777215))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/edit-notes.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbConnectionSettings.setIcon(icon1)
        self.pbConnectionSettings.setObjectName(_fromUtf8("pbConnectionSettings"))
        self.horizontalLayout_2.addWidget(self.pbConnectionSettings)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.pbCancel = QtGui.QPushButton(Login)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/cancel-icon-47512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbCancel.setIcon(icon2)
        self.pbCancel.setAutoDefault(False)
        self.pbCancel.setObjectName(_fromUtf8("pbCancel"))
        self.horizontalLayout_2.addWidget(self.pbCancel)
        self.pbOk = QtGui.QPushButton(Login)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/res/ok-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbOk.setIcon(icon3)
        self.pbOk.setDefault(True)
        self.pbOk.setObjectName(_fromUtf8("pbOk"))
        self.horizontalLayout_2.addWidget(self.pbOk)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(Login)
        QtCore.QMetaObject.connectSlotsByName(Login)

    def retranslateUi(self, Login):
        Login.setWindowTitle(_translate("Login", "Login", None))
        self.label.setText(_translate("Login", "Username:", None))
        self.label_2.setText(_translate("Login", "Password:", None))
        self.pbConnectionSettings.setText(_translate("Login", "...", None))
        self.pbCancel.setText(_translate("Login", "Cancel", None))
        self.pbOk.setText(_translate("Login", "OK", None))

import pricelist_rc
